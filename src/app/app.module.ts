import { NgModule }       from '@angular/core';
import { BrowserModule }  from '@angular/platform-browser';
import { FormsModule }    from '@angular/forms';
import { HttpModule, JsonpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';

//Routes
import { Routing } from './app.routes';

//Components

import { LoginComponent } from './Authentication/Login/login.component';
import { RegisterComponent } from './Authentication/Register/register.component';
import { AuthGuard } from './Authentication/Guards/auth.guard';
import { AuthenticationService } from './Authentication/Services';

//Modules
import { ModalModule } from 'angular2-modal';
import { BootstrapModalModule } from 'angular2-modal/plugins/bootstrap';
import { Ng2PaginationModule } from 'ng2-pagination'; 
import { DropdownModule } from "ngx-dropdown";

//Services

@NgModule({
  imports: [
      BrowserModule,
      FormsModule,
      HttpModule,
      JsonpModule,
      ModalModule,
      Routing,
      ModalModule.forRoot(),
      BootstrapModalModule,
      Ng2PaginationModule,
      DropdownModule
  ],
  declarations: [
    LoginComponent,
    RegisterComponent,
    AppComponent
  ],

  providers: [
      AuthenticationService,
      AuthGuard
  ],

  exports:[
    LoginComponent,
    RegisterComponent,
    AppComponent
  ],

  bootstrap: [ AppComponent ]
})
export class AppModule {
}
