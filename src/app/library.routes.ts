import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { MemberListComponent } from './Components/Member/List/member-list.component';
import { MemberDetailsComponent } from './Components/Member/Details/member-details.component';
import { MemberCreateComponent } from './Components/Member/Create/member-create.component';
import { HistoryComponent } from './Components/Shared/History/history.component';
import { BookListComponent } from './Components/Book/List/book-list.component';
import { BookTakeComponent } from './Components/Book/Take/book-take.component';
import { BookDetailsComponent } from './Components/Book/Details/book-details.component';
import { BookCreateComponent } from './Components/Book/Create/book-create.component';
import { LoginComponent } from './Components/Login/login.component';
import { RegisterComponent } from './Components/Register/register.component';
import { AuthGuard } from './Guards/auth.guard';
import { LibrarianDetailsComponent } from './Components/LibrarianDeitals/librarian-deitals.component';

// Route Configuration
export const routes: Routes = [
  { path: '',   redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent},
  { path: 'librariandeitals', component: LibrarianDetailsComponent, canActivate: [ AuthGuard ]},
  { path: 'books', component: BookListComponent, canActivate: [ AuthGuard ] },
  { path: 'books/:id', component: BookDetailsComponent, canActivate: [ AuthGuard ] },
  { path: 'books/take/:id', component: BookTakeComponent, canActivate: [ AuthGuard ] },
  { path: 'createbook', component: BookCreateComponent, canActivate: [ AuthGuard ] },
  { path: 'members', component: MemberListComponent, canActivate: [ AuthGuard ] },
  { path: 'members/:id', component: MemberDetailsComponent, canActivate: [ AuthGuard ] },
  { path: 'createmember', component: MemberCreateComponent, canActivate: [ AuthGuard ] },
  { path: 'history/:itemType/:id', component: HistoryComponent, canActivate: [ AuthGuard ] }  
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);