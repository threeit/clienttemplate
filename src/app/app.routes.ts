import { ModuleWithProviders }  from '@angular/core';
import { Routes, RouterModule, ActivatedRoute } from '@angular/router';
import { LoginComponent } from './Authentication/Login/login.component';
import { RegisterComponent } from './Authentication/Register/register.component';

// Route Configuration
export const routes: Routes = [
  { path: '',   redirectTo: '/login', pathMatch: 'full'},
  { path: 'login', component: LoginComponent},
  { path: 'register', component: RegisterComponent} 
];

export const Routing: ModuleWithProviders = RouterModule.forRoot(routes);