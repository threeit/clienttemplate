import { Component, ViewEncapsulation } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../Services';
import { User } from '../Model';

@Component({
    moduleId: module.id,
    templateUrl: 'register.component.html',
    styleUrls: [ 'register.component.scss' ],
    encapsulation: ViewEncapsulation.None
})

export class RegisterComponent {
    model: any = {};
    loading = false;
    showErrorMessage: boolean;
    errorMessage = "Registration failed!";

    constructor(
        private router: Router,
        private authenticationService: AuthenticationService) { }

    // Register defined user.
    register() {
        this.loading = true;
        this.authenticationService.addUser(this.model)
            .subscribe(
                    data => {
                        this.router.navigate(['/login']);
                    },
                    error => {
                        console.log(error);
                        this.showErrorMessage = true;
                        this.loading = false;
                    });
    }
}