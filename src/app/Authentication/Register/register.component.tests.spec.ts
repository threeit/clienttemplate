import { it, describe, expect, beforeEach } from 'angular2/testing';
import { RegisterComponent } from "./register.component";
import { Router, ActivatedRoute } from '@angular/router';
import { BaseRequestOptions, Http, HttpModule, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { Observable } from 'rxjs/Rx';
import { TestBed, async, inject } from '@angular/core/testing';
import { AuthenticationService } from "../Services";
import { MockAuthenticationService } from '../TestMocks';

describe('Register component testing...', () => {
    var _mockRouter = {
            navigate: jasmine.createSpy('navigate')
        };

    beforeEach(() => {
        _mockRouter.navigate.calls.reset();
        
        TestBed.configureTestingModule({
            providers: [
                { provide: AuthenticationService, useValue: new MockAuthenticationService()},
                { provide: Router, useValue: _mockRouter }
            ],
        });

        TestBed.compileComponents();
    });

    it('successfull register', async(inject(
        [AuthenticationService, Router], (authService, router) => {
            var registerComponent = new RegisterComponent(router, authService);
            registerComponent.register();

            expect(_mockRouter.navigate).toHaveBeenCalledWith(['/login']);
    })));
})