export class User {
    constructor(
        public uid: string,
        public firstName: string, 
        public lastName: string, 
        public displayName: string,
        public email: string,
        public password: string,
        public photoURL: string,
        public idCardNumber: string
        ){} 

    static CreateDeault() : User {
        return new User("", "", "", "", "", "", "", "");
    }   
}