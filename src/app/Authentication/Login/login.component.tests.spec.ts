import { it, describe, expect, beforeEach } from 'angular2/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { BaseRequestOptions, Http, HttpModule, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { TestBed, async, inject } from '@angular/core/testing';
import { Observable } from 'rxjs/Rx';
import { LoginComponent } from "./login.component"
import { User } from '../Model';
import { AuthenticationService } from "../Services";
import { MockAuthenticationService } from '../TestMocks';

describe('Login component testing...', () => {
    var _mockRouter = {
            navigateByUrl: jasmine.createSpy('navigate')
        };

    beforeEach(() => {
        localStorage.clear();
        _mockRouter.navigateByUrl.calls.reset();
        
        TestBed.configureTestingModule({
            providers: [
                { provide: AuthenticationService, useValue: new MockAuthenticationService()},
                { provide: Router, useValue: _mockRouter },
                { provide: ActivatedRoute, useValue: { 
                    params: Observable.of({id: 111}),
                    snapshot: {
                        queryParams: 'MockData'
                    }
                }}
            ],
        });

        TestBed.compileComponents();
    });

    it('go to librariandeitals if there is loged in user on nginit()', async(inject(
        [AuthenticationService, Router, ActivatedRoute],
        (authService, router, activetedRoute) => {
            localStorage.setItem("currentUser", "fakeData");
            
            var loginComponent = new LoginComponent(activetedRoute, router, authService);
            loginComponent.ngOnInit();

            expect(_mockRouter.navigateByUrl).toHaveBeenCalledWith('/librariandeitals');
    })));

    it('do not go to librariandeitals if there is no loged in user on nginit()', async(inject(
        [AuthenticationService, Router, ActivatedRoute],
        (authService, router, activetedRoute) => {
            
            var loginComponent = new LoginComponent(activetedRoute, router, authService);
            loginComponent.ngOnInit();
            
            expect(_mockRouter.navigateByUrl).not.toHaveBeenCalledWith('/librariandeitals');
    })));

    it('successfull login', async(inject(
        [AuthenticationService, Router, ActivatedRoute],
        (authService, router, activetedRoute) => {

            var loginComponent = new LoginComponent(activetedRoute, router, authService);
            loginComponent.login();

            expect(_mockRouter.navigateByUrl).not.toHaveBeenCalledWith('/books');
    })));
})