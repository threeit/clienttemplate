import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthenticationService } from '../Services';

@Component({
    moduleId: module.id,
    templateUrl: 'login.component.html',
    styleUrls: [ 'login.component.scss' ],
    encapsulation: ViewEncapsulation.None
})

export class LoginComponent implements OnInit {
    model: any = {};
    loading = false;
    returnUrl: string;
    showErrorMessage: boolean;
    errorMessage: string;

    constructor(
        private route: ActivatedRoute,
        private router: Router,
        private authenticationService: AuthenticationService,
        ) { }
    
    // Initialize component.
    ngOnInit() {
        // If there is logged in user, route to profile page.
        if(localStorage.getItem("currentUser")){
            this.router.navigateByUrl(this.route.snapshot.queryParams['returnUrl'] || '/librariandeitals');
        }

        // get return url from route parameters or default to '/'
        this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/books';
        this.showErrorMessage = false;
    }

    // Send login reqest to server using entered credentials.
    login() {
        this.authenticationService.login(this.model.email, this.model.password)
            .subscribe(
                data => {
                    this.router.navigateByUrl(this.returnUrl);
                },
                error => {
                    this.errorMessage = error._body;
                    this.showErrorMessage = true;
                }
            );
    }
}
