import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { Subject } from 'rxjs/Subject';
import 'rxjs/add/operator/map'
import { User } from '../Model';
import { environment } from '../../../environments/environment';
import { Router, ActivatedRoute } from '@angular/router';

@Injectable()
export class AuthenticationService {
    private logged: Subject<boolean> = new Subject<boolean>();
    private token: string;

    constructor(
        private http: Http,
        private router: Router,
        private route: ActivatedRoute) { 
            // set token if saved in local storage
            var currentUser = JSON.parse(localStorage.getItem('currentUser'));
            this.token = currentUser && currentUser.token;
    }

    private authUrl = environment.domain + '/authenticate/';
    private addUserUrl = environment.domain + '/addUser/';    
    private editUserUrl = environment.domain + '/api/editUser/';        

    // Send login request to server.
    login(email: string, password: string): Observable<any> {
        return this.http.post(this.authUrl, { email: email, password: password })
        .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user && user.token) {
                    // set public token property
                    this.token = user.token;
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.logged.next(true);
                }
            });
    }

    // Send request for adding user to server.
    addUser(user: User){
        return this.http.post(this.addUserUrl, user);
    }
    
    // Send request for editing user to server.    
    editUser(user: User): Observable<any>{
        let options = this.getAuthorizationHeader();
        return this.http.post(this.editUserUrl, user, options)
        .map((response: Response) => {
                // login successful if there's a jwt token in the response
                let user = response.json();
                if (user) {
                    // store user details and jwt token in local storage to keep user logged in between page refreshes
                    localStorage.setItem('currentUser', JSON.stringify(user));
                    this.logged.next(true);
                }
            });
    }

    // Logout user.   
    logout() {
        // Remove user from local storage to log user out.
        localStorage.removeItem('currentUser');
        this.router.navigateByUrl(this.route.snapshot.queryParams['returnUrl'] || '/login');
        this.logged.next(false);
    }

    // Expose login event to other components.
    getLogged(): Observable<boolean> {
        return this.logged.asObservable();
    }

    // Handle authorization header.
    getAuthorizationHeader() {
        // Add authorization header with jwt token.
        let headers = new Headers({ 'Authorization': 'Bearer ' + this.token });
        let options = new RequestOptions({ headers: headers });

        return options;
     }

     // Append authorization header.
     appendAuthorizationHeader(options: RequestOptions) {
         options.headers.append('Authorization', 'Bearer ' + this.token);
         return options;
     }
}