import { it, describe, expect, beforeEach } from 'angular2/testing';
import { BaseRequestOptions, Http, HttpModule, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { TestBed, async, inject } from '@angular/core/testing';
import { User } from '../Model';
import { AuthenticationService } from "./authentication.service";
import { Router, ActivatedRoute } from '@angular/router';

describe('AuthenticationService testing...', () => {

    let _initialLibrarian = {
                uid: "111",
                email: "capetan@ahab.com", 
                password: "Levithian", 
                photoURL: "https://upload.wikimedia.org/wikipedia/en/c/ca/Leviathan_special_edition_cover.jpg",
                displayName: "Ahab",
                idCardNumber: "111",
                token: "customTokenInfo"
            };

    let _mockBackend = new MockBackend();
    
    beforeEach(() => {
        localStorage.clear();

        TestBed.configureTestingModule({
            providers: [
                AuthenticationService,
                MockBackend,
                BaseRequestOptions,
                {
                    provide: Http,
                    useFactory: (backend, options) => new Http(backend, options),
                    deps: [MockBackend, BaseRequestOptions]
                },
                {
                    provide: Router, 
                    useValue: { navigateByUrl: jasmine.createSpy('navigate') } 
                },
                {
                    provide: ActivatedRoute, 
                    useValue: { 
                        snapshot: {
                            params: { id: 'testId' }
                        }
                    }
                }
            ],
            imports: [
                HttpModule
            ]
        });

        TestBed.compileComponents();

        _mockBackend.connections.subscribe(conn => {
                conn.mockRespond(
                    new Response(
                        new ResponseOptions({
                            body: _initialLibrarian
                        })));
            });
    });
 
    it('stored good login informationa after login.', async(inject(
        [AuthenticationService, Router, ActivatedRoute, MockBackend], (authService, router, activatedRoute, _mockBackend) => {
            var temp;

            authService
            .getLogged()
            .subscribe(data => {
                temp = data;
            });

            authService.login(_initialLibrarian.email, _initialLibrarian.password)
            .subscribe(
                data => {
                    expect(localStorage.getItem("currentUser")).toBeTruthy();
                    expect(JSON.parse(localStorage.getItem("currentUser")).uid).toBe(_initialLibrarian.uid);
                    expect(JSON.parse(localStorage.getItem("currentUser")).email).toBe(_initialLibrarian.email);
                    expect(JSON.parse(localStorage.getItem("currentUser")).password).toBe(_initialLibrarian.password);
                    expect(JSON.parse(localStorage.getItem("currentUser")).photoURL).toBe(_initialLibrarian.photoURL);
                    expect(JSON.parse(localStorage.getItem("currentUser")).displayName).toBe(_initialLibrarian.displayName);
                    expect(JSON.parse(localStorage.getItem("currentUser")).idCardNumber).toBe(_initialLibrarian.idCardNumber);
                    expect(JSON.parse(localStorage.getItem("currentUser")).token).toBe(_initialLibrarian.token); 
                    expect(temp).toBe(true);                   
                },
                error => {
                    console.log(error);
                    expect(localStorage.getItem("currentUser")).toBeTruthy();
                }
            );
    })));

    it('localStorage cleaned after logout.', async(inject(
        [AuthenticationService, Router, ActivatedRoute, MockBackend], (authService, router, activatedRoute, _mockBackend) => {
            var temp;

            authService
            .getLogged()
            .subscribe(data => {
                temp = data;
            });

            authService.login(_initialLibrarian.email, _initialLibrarian.password)
            .subscribe(
                data => {
                    authService.logout();
                    expect(localStorage.getItem("currentUser")).toBeNull();  
                    expect(temp).toBe(false);              
                }
            );
            
    })));

     it('correct information stored after edit user.', async(inject(
        [AuthenticationService, Router, ActivatedRoute, MockBackend], (authService, router, activatedRoute, _mockBackend) => {
            var temp;

             authService
            .getLogged()
            .subscribe(data => {
                temp = data;
            });

            authService.login(_initialLibrarian.email, _initialLibrarian.password)
            .subscribe(
                data => {
                    var editLibrarian = User.CreateDeault();
                    editLibrarian.displayName = "Moby Dick";
                    editLibrarian.email = "no@harpoons.com" ;

                    _initialLibrarian.displayName = editLibrarian.displayName;
                    _initialLibrarian.email = editLibrarian.email;                    

                    authService.editUser(editLibrarian)
                    .subscribe(
                        response => {
                            expect(JSON.parse(localStorage.getItem("currentUser")).displayName).toBe(editLibrarian.displayName);
                            expect(JSON.parse(localStorage.getItem("currentUser")).email).toBe(editLibrarian.email);
                            expect(temp).toBe(true);                                       
                        }
                    );
                },
                error => {
                    console.log(error);
                    expect(localStorage.getItem("currentUser")).toBeTruthy();
                }
            );
            
    })));

    it('adding user successfull.', async(inject(
        [AuthenticationService, Router, ActivatedRoute, MockBackend], (authService, router, activatedRoute, _mockBackend) => {
            authService.addUser(_initialLibrarian)
            .subscribe(
                 data => {
                     expect(true).toBeTruthy();
                 },
                 error => {
                     expect(true).toBeFalsy();
                 }
            )
    })));
});

