import { Observable } from 'rxjs/Rx';
import { BaseRequestOptions, Http, HttpModule, Response, ResponseOptions } from '@angular/http';
import { User } from '../Model';
import { AuthenticationService } from '../Services';

export class MockAuthenticationService extends AuthenticationService{
    _mockResponse = new Response(new ResponseOptions({
        body: "mockdata", 
    }));

    isLogged = true;

    constructor(){
        super(null);
    }

    login(email: string, password: string) {
        return Observable.of(this._mockResponse);
    }

    addUser(user: User) {
        return Observable.of(this._mockResponse);
    }

    editUser(user: User){
        return Observable.of(this._mockResponse);
    }

    logout() {
        return Observable.of(false);
    }

    getLogged(): Observable<boolean> {
        return Observable.of(this.isLogged);
    }
}