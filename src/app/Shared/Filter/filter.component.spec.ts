import { it, describe, expect, beforeEach } from 'angular2/testing';
import { BaseRequestOptions, Http, HttpModule, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { TestBed, async, inject } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { FilterComponent } from "./filter.component";

describe('FilterComponent testing...', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ ],
        });

        TestBed.compileComponents();
    });

    it('check property values after receiving onChanges event', async(inject(
        [], () => {
            var filterComponent = new FilterComponent();

            var event = {
                fieldDescriptions: {
                     currentValue : [{ label: 'labelToBeShown', value : 1 }]
                 }
            };
            filterComponent.ngOnChanges(event);

            expect(filterComponent.fieldDescriptions).toBeDefined();
            expect(filterComponent.fieldDescriptions[0].label).toEqual('labelToBeShown');
            expect(filterComponent.fieldDescriptions[0].value).toEqual(1);
    })));

    it('should set to undefined all filterdescription values on reset', async(inject(
        [], () => {
            var filterComponent = new FilterComponent();

            var event = {
                fieldDescriptions: {
                     currentValue : [{ label: 'labelToBeShown', value : 1 }]
                 }
            };
            filterComponent.ngOnChanges(event);
            filterComponent.reset();

            expect(filterComponent.fieldDescriptions).toBeDefined();
            expect(filterComponent.fieldDescriptions[0].label).toEqual('labelToBeShown');
            expect(filterComponent.fieldDescriptions[0].value).toEqual(undefined);
    })));
});