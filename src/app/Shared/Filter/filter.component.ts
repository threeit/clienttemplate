import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'filter',
    moduleId: module.id,
    templateUrl: 'filter.component.html'
})
export class FilterComponent implements OnChanges {

    @Input() fieldDescriptions: any = [];
    @Output() applyFiltering = new EventEmitter();
    public isCollapsedContent: boolean = false;

    constructor() {}

    // Reset filter info.
    reset(){
        this.fieldDescriptions.forEach(element => {
            element.value = undefined;
        });
        this.filter();
    }

    // Filter data.
    filter(){
        var filterCondition = {};
        this.fieldDescriptions.forEach(element => {
            filterCondition[element.propertyName] = element.value;
        });

        this.applyFiltering.emit(filterCondition);
    }

    ngOnChanges(changes:any) {
        this.fieldDescriptions = changes.fieldDescriptions.currentValue;
    }
 }
