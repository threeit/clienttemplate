import { Component, OnInit, Input, Output, OnChanges, EventEmitter } from '@angular/core';
import { Observable } from 'rxjs/Observable';

@Component({
    selector: 'list-view',
    moduleId: module.id,
    templateUrl: 'list-view.component.html'
})
export class ListViewComponent implements OnChanges {

    @Input() items: any = [];
    @Input() editable: boolean = true;
    @Output() itemsChanged = new EventEmitter();
    public currentItem: any = '';

    constructor() 
    {
        this.items = [];
    }

    removeItem(itemToRemove:any){
        for(var i = this.items.length - 1; i >= 0; i--){
            if(this.items[i] == itemToRemove)
                this.items.splice(i, 1);
        }

        this.itemsChanged.emit(this.items);
    }

    addItem(){
        if(!this.currentItem) 
            return;
        if(!this.items)
            this.items = [];
        this.items.push(this.currentItem);
        this.currentItem = '';
        this.itemsChanged.emit(this.items);
    }

    ngOnChanges(changes:any) {
        if(changes.items)
            this.items = changes.items.currentValue;
        if(changes.editable)
            this.editable = changes.editable.currentValue;
    }
 }
