import { it, describe, expect, beforeEach } from 'angular2/testing';
import { BaseRequestOptions, Http, HttpModule, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { TestBed, async, inject } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { ListViewComponent } from "./list-view.component";

describe('ListViewComponent testing...', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [ ],
        });

        TestBed.compileComponents();
    });

    it('correctly change values after receiving onChanges event', async(inject(
        [], () => {
            var event = {
                items : {
                    currentValue : ['test']
                }, 
                editable : {
                    currentValue : true
                }
            }

            var filterComponent = new ListViewComponent();
            filterComponent.ngOnChanges(event);

            expect(filterComponent.items).toEqual(['test']);
            expect(filterComponent.items.length).toEqual(1);
            expect(filterComponent.items[0]).toEqual('test');
    })));

    it('correctly change values after receiving onChanges event', async(inject(
        [], () => {
            var event = {
                items : {
                    currentValue : ['test']
                }, 
                editable : {
                    currentValue : true
                }
            }

            var filterComponent = new ListViewComponent();
            filterComponent.ngOnChanges(event);

            expect(filterComponent.items).toEqual(['test']);
            expect(filterComponent.items.length).toEqual(1);
    })));

    it('correctly add new item', async(inject(
        [], () => {
            var filterComponent = new ListViewComponent();
            filterComponent.currentItem = 'new item';
            filterComponent.addItem();

            expect(filterComponent.items).toEqual(['new item']);
            expect(filterComponent.items.length).toEqual(1);
            expect(filterComponent.currentItem).toEqual('');
    })));

    it('correctly removes item', async(inject(
        [], () => {
            var filterComponent = new ListViewComponent();
            filterComponent.currentItem = 'new item';
            filterComponent.addItem();
            filterComponent.removeItem('new item');

            expect(filterComponent.items).toEqual([]);
            expect(filterComponent.items.length).toEqual(0);
    })));
});