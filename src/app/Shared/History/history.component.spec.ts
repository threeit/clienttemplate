import { it, describe, expect, beforeEach } from 'angular2/testing';
import { BaseRequestOptions, Http, HttpModule, Response, ResponseOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { TestBed, async, inject } from '@angular/core/testing';
import { Router, ActivatedRoute } from '@angular/router';
import { HistoryComponent } from "./history.component";
import { MockOverlay, MockBookService, MockModalDialog, MockMemberService, MockHistoryService } from '../../../TestMocks';
import { BookService, HistoryService, MemberService } from '../../../Services';
import { History, Member } from '../../../Model';

describe('HistoryComponent testing...', () => {
    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                { provide: BookService, useValue: new MockBookService()},
                { provide: MemberService, useValue: new MockMemberService()},
                { provide: HistoryService, useValue: new MockHistoryService()},
                { provide: ActivatedRoute, useValue: { 
                    snapshot: {
                        params: { id: 'testId' }
                    }
                }},
            ],
        });

        TestBed.compileComponents();
    });

    it('check property values after receiving onChanges event', async(inject(
        [BookService, MemberService, HistoryService, ActivatedRoute],
        (bookService, memberService, historyService, activatedRoute) => {

            var historyComponent = new HistoryComponent(historyService, bookService, memberService, activatedRoute);
            historyComponent.ngOnInit();

            expect(historyComponent.historyEntries.length).toEqual(1);//don't care about mock data in historyEntries
    })));

    it('loads history entries for Member', async(inject(
        [BookService, MemberService, HistoryService, ActivatedRoute],
        (bookService, memberService, historyService, activatedRoute) => {

            var historyComponent = new HistoryComponent(historyService, bookService, memberService, activatedRoute);
            historyComponent.ngOnInit();

            var params = { id: 1, itemType: 'member'};
            historyComponent.loadHistory(params)

            expect(historyComponent.historyEntries.length).toEqual(1);
            expect(historyComponent.member).toBeDefined();
            expect(historyComponent.member._id).toEqual('member1');
    })));

    it('loads history entries for Book', async(inject(
        [BookService, MemberService, HistoryService, ActivatedRoute],
        (bookService, memberService, historyService, activatedRoute) => {

            var historyComponent = new HistoryComponent(historyService, bookService, memberService, activatedRoute);
            historyComponent.ngOnInit();

            var params = { id: 1, itemType: 'book'};
            historyComponent.loadHistory(params)

            expect(historyComponent.historyEntries.length).toEqual(1);
            expect(historyComponent.member).toBeDefined();
            expect(historyComponent.book._id).toEqual('book1');
    })));

    it('displays UnknownMember if member is not correctly set in history entry', async(inject(
        [BookService, MemberService, HistoryService, ActivatedRoute],
        (bookService, memberService, historyService, activatedRoute) => {

            var historyComponent = new HistoryComponent(historyService, bookService, memberService, activatedRoute);
            historyComponent.ngOnInit();

            var historyEntry = History.CreateDefault();
            var retval = historyComponent.getMemberName(historyEntry)

            expect(retval).toEqual('Unknown member');
    })));

    it('displays full name if member is correctly set in history entry', async(inject(
        [BookService, MemberService, HistoryService, ActivatedRoute],
        (bookService, memberService, historyService, activatedRoute) => {

            var historyComponent = new HistoryComponent(historyService, bookService, memberService, activatedRoute);
            historyComponent.ngOnInit();

            var historyEntry = History.CreateDefault();
            historyEntry.Member = Member.CreateDeault();
            historyEntry.Member.firstName = 'Some';
            historyEntry.Member.lastName = 'One';
            var retval = historyComponent.getMemberName(historyEntry)

            expect(retval).toEqual('Some One');
    })));
});