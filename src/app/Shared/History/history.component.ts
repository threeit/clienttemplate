import { Component, OnInit, ViewContainerRef, OnChanges, ViewEncapsulation } from '@angular/core';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Observable } from 'rxjs/Observable';
import { BookService, MemberService, HistoryService } from '../../../Services';
import { History, Book, Member } from '../../../Model';

@Component({
    moduleId: module.id,
    templateUrl: 'history.component.html',
    encapsulation: ViewEncapsulation.None
})
export class HistoryComponent implements OnInit {
    constructor(private historyService: HistoryService, private bookService: BookService, private memberService: MemberService, 
                private route: ActivatedRoute) {}

    historyEntries: History[];
    book: Book = undefined;
    member: Member = undefined;

    // Initialize component.
    ngOnInit(){
        this.loadHistory(this.route.snapshot.params)
            .subscribe(
                historyEntries => this.setHistoryEntries(historyEntries),
                err => console.log(err));
    }

    setHistoryEntries(historyEntries){
        this.historyEntries = historyEntries;
    }

    // Request history information form server/
    loadHistory(params: Params){
        var itemType = params['itemType'];
        var id = params['id'];
        if(itemType == 'book')
        {
            this.bookService.getBookById(id)
                            .subscribe(
                                book => this.book = book, 
                                err => console.log(err));
            return this.historyService.getBookHistory(id);
        }

        this.memberService.getMemberByCardNumber(id)
                          .subscribe(
                              member => this.member = member, 
                              err => console.log(err));
        return this.historyService.getMemberHistory(id);
    }

    // Setup member name.
    getMemberName(entry: History){
        return entry.Member 
                    ? entry.Member.firstName + ' ' + entry.Member.lastName 
                    : 'Unknown member';
    }
}
