import { LibrariNGCliPage } from './app.po';

describe('librari-ngcli App', function() {
  let page: LibrariNGCliPage;

  beforeEach(() => {
    page = new LibrariNGCliPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
